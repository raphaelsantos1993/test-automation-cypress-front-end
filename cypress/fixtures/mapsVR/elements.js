export const elements = {
    buttonCloseCookies: '.lum-content>a[href="#"]',
    linkOndeAceita: 'a[href="/onde-aceita.htm"]',
    buttonAbrirMenuSecundario: 'Abrir menu secundário',
    linkOndeUsarVR: 'Onde usar meu cartão VR',
    mapaGoogle: '#mapSection',
    textTrabalhador: 'Sou Trabalhador',
    urlViewportInfo: 'https://maps.googleapis.com/maps/*'
};
