// cypress/support/commands/visitHomePage.js
Cypress.Commands.add('visitHomePage', (baseUrl) => {
    cy.visit(baseUrl);
    cy.window().should('have.property', 'document').should('be.exist');
    cy.get(elements.buttonCloseCookies).click({ force: true });
});

// cypress/support/commands/navigateToPraVoceSection.js
Cypress.Commands.add('navigateToPraVoceSection', (linkSelector, textTrabalhador, buttonAbrirMenuSecundario) => {
    cy.get(linkSelector)
        .contains(textTrabalhador)
        .focus()
        .next()
        .contains(buttonAbrirMenuSecundario)
        .click({ force: true });
});

// cypress/support/commands/clickOnOndeUsarVRLink.js
Cypress.Commands.add('clickOnOndeUsarVRLink', (viewportInfoUrl, linkText) => {
    cy.intercept('GET', viewportInfoUrl).as('viewportInfo');
    cy.contains(linkText).click();
});

// cypress/support/commands/verifyMapDisplayed.js
Cypress.Commands.add('verifyMapDisplayed', (mapSelector) => {
    cy.wait('@viewportInfo').then(() => {
        cy.get(mapSelector).should('be.visible');
    });
});
