import { Given, When, Then } from 'cypress-cucumber-preprocessor/steps';
import { elements } from '../../../fixtures/mapsVR/elements';


Given('que estou na página inicial do site VR', () => { // Configuração inicial do teste: visitando a página inicial do site VR
  cy.visit(Cypress.env('baseUrl'));
  cy.window().should('have.property', 'document').should('be.exist');  // Verifica se o documento está carregado na página
  cy.get(elements.buttonCloseCookies).click({ force: true }); // Clica em um link específico na página, utilizando a opção force para garantir o clique

  //cy.visitHomePage(Cypress.env('baseUrl')); 
  // *caso queira usar os Commads do cypress 
});


When('eu navego para a seção "Pra Você"', () => { // Ação do usuário: navegar para a seção "Pra Você"
  cy.get(elements.linkOndeAceita) // Localiza o link com o atributo href igual a "/onde-aceita.htm"
    .contains(elements.textTrabalhador) // Localiza o texto "Sou Trabalhador" dentro do link
    .focus() // Dá foco ao link
    .next() // Seleciona o próximo elemento após o link
    .contains(elements.buttonAbrirMenuSecundario) // Localiza o texto "Abrir menu secundário" dentro do próximo elemento
    .click({ force: true }); // Clica no elemento "Abrir menu secundário", forçando o clique se necessário

  //cy.navigateToPraVoceSection(elements.linkOndeAceita, elements.textTrabalhador, elements.buttonAbrirMenuSecundario);
  // *caso queira usar os Commads do cypress 
});


When('clico no link "Onde usar meu cartão VR"', () => { // Ação do usuário: clicar no link "Onde usar meu cartão VR"
  cy.intercept('GET', elements.urlViewportInfo).as('viewportInfo'); // Intercepta uma chamada de API para o Google Maps
  cy.contains('Onde usar meu cartão VR').click(); // Clica no link com o texto "Onde usar meu cartão VR"

  //cy.clickOnOndeUsarVRLink(elements.urlViewportInfo, 'Onde usar meu cartão VR');
  // *caso queira usar os Commads do cypress
});


Then('devo ver o mapa do Google sendo exibido em tela', () => {
  cy.wait('@viewportInfo').then(() => { // Verificação do comportamento esperado: verificar se o mapa do Google está sendo exibido
    cy.get(elements.mapaGoogle).should('be.visible');

    //cy.verifyMapDisplayed(elements.mapaGoogle);
    // *caso queira usar os Commads do cypress
  });
});
