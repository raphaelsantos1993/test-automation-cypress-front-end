#language: pt

Funcionalidade: Verificar o mapa do Google ao clicar em Onde usar meu cartão VR

Como usuário do site VR
Eu quero verificar se o mapa do Google abre ao clicar no botão "Onde usar meu cartão VR"
Para encontrar locais onde posso usar meu cartão VR

Cenário: Verificar se o mapa do Google é exibido ao clicar no botão "Onde usar meu cartão VR"
Dado que estou na página inicial do site VR
Quando eu navego para a seção "Pra Você"
E clico no link "Onde usar meu cartão VR"
Então devo ver o mapa do Google sendo exibido em tela
