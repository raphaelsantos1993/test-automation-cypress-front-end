# Projeto Automação Cypress  - Frontend

## Descrição

Este projeto em Cypress foi desenvolvido para realizar testes automatizados no frontend do site www.vr.com.br. O objetivo é validar o funcionamento correto das principais funcionalidades da página, como navegação, interação com elementos e validação de conteúdo.

## Tecnologias Utilizadas

- [Cypress](https://docs.cypress.io/guides/overview/why-cypress.html)
- [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide)
- [Node.js](https://nodejs.org/en)

## Pré-requisitos

Antes de executar o projeto, certifique-se de ter as seguintes ferramentas instaladas em seu sistema:

- [Node.js](https://nodejs.org/): O Node.js é necessário para executar o projeto. Você pode baixar e instalar a versão adequada para o seu sistema operacional a partir do site oficial.

- [Cypress](https://www.cypress.io/): O Cypress é uma ferramenta de automação de testes end-to-end para aplicações web. Certifique-se de ter o Cypress instalado globalmente em seu sistema.

## Como Executar os Testes

### Passos
1. Clone este repositório para o seu ambiente local, execute o seguinte comando no terminal:
   ```bash
   git clone https://github.com/seu-usuario/nome-do-repositorio.git
   ```
 2. Navegue até o diretório do projeto:
   ```bash
   cd teste_frontend
   ```
   
3. Instale as dependências do projeto com npm, execute o seguinte comando no terminal::
   ```bash
   npm install
   ```  
4. Para executar os cenarios de testes Funcionais na Automação **(Cypress)** use os seguintes comandos:

**test:open**: Abre o Cypress no modo gráfico. Para executar este comando, abra o terminal e digite:
 ```bash
npm run test:open
   ```  

 **test:headless**: Executa o Cypress em modo headless, sem interface gráfica. Para executar este comando, abra o terminal e digite:
 ```bash
npm run test:headless
   ```  

## Estrutura do Projeto

```sql
O projeto está estruturado da seguinte forma:

test-automation-frontend/
│
├── cypress/                       // Pasta principal do Cypress
│   │
│   └── e2e/                       // Testes end-to-end
│       │
│       └── features/              // Pasta para os features
│           ├── mapsVR.feature    // Arquivo de feature para o mapsVR
│           └── mapsVR/            // Pasta para os testes relacionados ao mapsVR
│               │
│               └── mapsVR.js      // Arquivo para os testes do mapsVR
│               
│
├── fixtures/                      // Dados para os testes
│   │
│   └── mapsVr/                    // Pasta para os dados relacionados ao mapsVR
│       │
│       └── elements.js            // Arquivo para os elementos do mapsVR
│
├── support/                       // Suporte aos testes
│   │
│   ├── commands.js                // Comandos personalizados do Cypress
│   ├── commands_mapsVr.js         // Comandos personalizados específicos para o mapsVR
│   │
│   └── e2e.js                     // Configurações ou scripts adicionais para os testes end-to-end
│
├── node_modules/                  // Módulos do Node.js
├── .gitignore                      // Arquivo de configuração para o Git
├── cypress.config                  // Configurações do Cypress
├── cypress.env.json                // Variáveis de ambiente do Cypress
├── package-lock.json               // Informações detalhadas das dependências do projeto
└── package.json                    // Informações e dependências do projeto

```

## Estrutura do Projeto

1. **cypress/**
   - **e2e/**
     - **features/**
       - `mapsVR.feature`: Arquivo de feature para o mapsVR
     - **mapsVR/**
       - `mapsVR.js`: Arquivo para os testes do mapsVR
   - **fixtures/**
     - **mapsVr/**
       - `elements.js`: Arquivo para os elementos do mapsVR
   - **support/**
     - `commands.js`: Comandos personalizados do Cypress
     - `commands_mapsVr.js`: Comandos personalizados específicos para o mapsVR
     - `e2e.js`: Configurações ou scripts adicionais para os testes end-to-end

2. **node_modules/**: Módulos do Node.js
3. **.gitignore**: Arquivo de configuração para o Git
4. **cypress.config**: Configurações do Cypress
5. **cypress.env.json**: Variáveis de ambiente do Cypress
6. **package-lock.json**: Informações detalhadas das dependências do projeto
7. **package.json**: Informações e dependências do projeto

--------------------------------------------------------------------------------------------------------------------

## Cenarios de Teste Ultilizado BDD:

```gherkin
Funcionalidade: Verificar o mapa do Google ao clicar em Onde usar meu cartão VR

Como usuário do site VR
Eu quero verificar se o mapa do Google abre ao clicar no botão "Onde usar meu cartão VR"
Para encontrar locais onde posso usar meu cartão VR

Cenário: Verificar se o mapa do Google é exibido ao clicar no botão "Onde usar meu cartão VR"
Dado que estou na página inicial do site VR
Quando eu navego para a seção "Pra Você"
E clico no link "Onde usar meu cartão VR"
Então devo ver o mapa do Google sendo exibido em tela

```
--------------------------------------------------------------------------------------------------------------------

